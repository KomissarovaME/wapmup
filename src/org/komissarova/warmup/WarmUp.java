package org.komissarova.warmup;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Задача разминка. Требуется определить максимальную прибыль для компании по известным 
 * времени и стоимости заказов
 * 
 * Алгоритм: получить исходные данные, отсортировать массив заказов по возрастанию, 
 * найти прибыль от самых выгодных (самых последних в массиве) заказов
 * 
 * @author Комиссарова М.Е., 16ит18К
 */
public class WarmUp {
    private static final String RESULT_FORMAT = "За время %d компания сможет выполнить %d заказов. Максимальная прибыль составит %d";
    private static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        /**  
         * Получение исходных данных 
         */
        System.out.println("Введите доступное для компании время: ");
        int time = scanner.nextInt();
        
        System.out.println("Введите кол-во доступных заказов: ");
        int orders_count = scanner.nextInt();
        
        int[] orders = new int[orders_count];
        for(int i = 0; i < orders_count; i++) {
            System.out.printf("Введите прибыль от %d заказа: ", i + 1);
            orders[i] = scanner.nextInt();
        }
        
        /**
         * Сортировка массива по возрастанию
         */
        Arrays.sort(orders);
        
        /**
         * На самом деле кол-во заказов, которые компания может выполнить зависит от двух величин
         * и равно наименьшей из них. Т. о. елси компании доступно время time и count_orders заказов
         * то компания сможет выполнить Math.min(orders_count, time)
         * 
         * Пример: time = 4, orders_count = 2
         * Времени у компании полно, однако заказов всего два.
         * Возможна и обратная ситуация, когда заказов больше, чем времени. Тогда нужно выполнить самые лакомые
         * заказы для получения максимального дохода
         */
        orders_count = Math.min(orders_count, time);
        int profit = 0;
        
        /**
         * Подсчёт прибыли путём сложения прибыли от самых выгодных заказов
         */
        for(int i = 1; i <= orders_count; i++) {
            profit += orders[orders.length - i];
        }
        
        /**
         * Форматированный вывод результатов работы
         */
        System.out.printf(RESULT_FORMAT, time, orders_count, profit);
    }
}
